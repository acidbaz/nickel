package com.nickel.exercise

import org.junit.jupiter.api.Test

class MainTest {

  @Test
  def testBasket(): Unit = {
    val books = List(
      Book(1, 1),
      Book(2, 1),
      Book(3, 2),
      Book(4, 2),
      Book(5, 3),
      Book(6, 3),
      Book(7, 4),
      Book(8, 5)
    )
    val basket = Basket(books)
    val price = Main.getBasketPrice(basket)
    println(price)
    assert(price == BigDecimal("51.20"))
  }

  @Test
  def testBasket2(): Unit = {
    val books = List(
      Book(1, 1),
      Book(2, 2),
      Book(3, 3),
      Book(4, 1),
      Book(5, 2),
      Book(6, 3),
      Book(7, 1),
      Book(8, 2),
      Book(9, 3),
      Book(10, 4),
      Book(11, 5)
    )
    val basket = Basket(books)
    val price = Main.getBasketPrice(basket)
    println(price)
    assert(price == BigDecimal("72.80"))
  }

  @Test
  def testEmptyBasket = {
    val price = Main.getBasketPrice(Basket(List.empty[Book]))
    assert(price == 0)

  }
  @Test
  def testTransfer() = {
    val list3 = List(Book(1, 1), Book(2, 3), Book(3, 5))
    val list5 = List(Book(4, 1), Book(5, 2), Book(6, 3), Book(7, 4), Book(8, 5))
    System.out.println(Main.transferBook(list5, list3).map(_.map(_.number)))
  }

}
