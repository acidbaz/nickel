package com.nickel.exercise

import java.security.InvalidParameterException

object Main {

  /**
   * Computes the price for a given basket, maximizing discounts.
   *
   * @param basket the basket
   * @return the price of the basket
   */
  def getBasketPrice(basket: Basket): BigDecimal = {
    // First we create sublists of different books
    val sublists = extractSublists(basket)
    // Then we optimize it
    val optimizedSublists = optimizeSublists(sublists)
    // Finally we multiply every sublist's size by the associated factor to apply the discount and by the book unit price
    optimizedSublists.map { s =>
      val factor: BigDecimal = factors.getOrElse(s.size, BigDecimal("1.00"))
      factor * bookPrice * BigDecimal(s.size.toString)
    }.sum
  }

  /**
   * Transfers a book, that is NOT in list2, from list1 to list2
   *
   * @param list1 the list of 5 elements
   * @param list2 the list of 3 elements
   * @return a list containing the thow lists
   */
  def transferBook(list1: List[Book], list2: List[Book]): List[List[Book]] = {
    val book = list1.filter(b => !list2.map(_.number).contains(b.number)).head
    val newList2 = list2 :+ book
    val newList1 = list1.filter(_.id != book.id)

    List(newList1, newList2)
  }

  /**
   * Optimize the discounts for the given sublists
   *
   * @param sublists the sublists to optimize
   * @return the optimized sublists
   */
  def optimizeSublists(sublists: List[List[Book]]): List[List[Book]] = {
    // separate list of size 3, 5 and others that don't need optimization
    val fiveBooksLists = sublists.filter(_.size == 5)
    val threeBooksLists = sublists.filter(_.size == 3)
    val otherLists: List[List[Book]] = sublists.filter(s => s.size != 5 && s.size != 3)

    // get the lowest size
    val index = if (threeBooksLists.size < fiveBooksLists.size) threeBooksLists.size else fiveBooksLists.size

    // split at the given index
    val splittedFive = fiveBooksLists.splitAt(index)
    val splittedThree = threeBooksLists.splitAt(index)

    // take the first part of the split, to guarantee we have the same size
    val merged = mergeSublists(splittedFive._1, splittedThree._1)

    splittedFive._2 // the rest of the splitted list of size = 5 (may be empty)
      .concat(splittedThree._2) // the rest of the splitted list of size = 3 (may be empty)
      .concat(otherLists) // the lists of size = 1 / 2 / 4
      .concat(merged) // the merged lists of size = 3 & 5
  }

  /**
   * Merge lists of size = 5 & 3 into two lists of size = 4 to get a better discount
   * @param fiveBooksLists the lists of size = 5
   * @param threeBooksLists the lists of size = 3
   * @return the optimized lists of size = 4
   */
  def mergeSublists(fiveBooksLists: List[List[Book]], threeBooksLists: List[List[Book]]) = {
    if (fiveBooksLists.size != threeBooksLists.size) throw new InvalidParameterException("The sublists don't have the same size.")
    else {
      (fiveBooksLists zip threeBooksLists).flatMap {
        case (fiveSublist, threeSublist) =>
          transferBook(fiveSublist, threeSublist)
      }
    }
  }

  /**
   * Creates a list of sublists for a given basket. Each sublist contains different books. Maximizes the size of the sublists
   * @param basket the given basket
   * @return A list of sublists of differents books according to their number
   */
  def extractSublists(basket: Basket): List[List[Book]] = {
    var startingList = basket.books
    var sublists: List[List[Book]] = List.empty

    // Dispatch
    while (startingList.nonEmpty) {
      var insertedKeys: List[Int] = List.empty // the numbers of the books we already inserted in the sublists

      val sublist: List[Book] = startingList.flatMap { b =>
        if (!insertedKeys.contains(b.number)) {
          insertedKeys = insertedKeys :+ b.number
          Some(b)
        } else None
      }
      sublists = sublists :+ sublist
      startingList = startingList.diff(sublist)
      insertedKeys = List.empty
    }
    sublists
  }
}
