package com.nickel

package object exercise {

  /**
   *
   * @param id
   * @param number the number of the book (1 / 2 / 3 / 4 / 5 )
   */
  case class Book(id: Int, number : Int)

  case class Basket(books: List[Book])

  final val bookPrice : BigDecimal = BigDecimal("8.00")

  /**
   * Represents the discounts, we use bigDecimal for precision.
   */
  final val factors : Map[Int, BigDecimal] = Map(
    2 -> BigDecimal("0.95"),
    3 -> BigDecimal("0.90"),
    4 -> BigDecimal("0.80"),
    5 -> BigDecimal("0.75"),
  )
}
